package com.example.demo.domain;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Team {

	@Id 
	@GeneratedValue
	Long id;
	String name;
	String locaion;
	String mascoote;
	@OneToMany(cascade=CascadeType.ALL) @JoinColumn(name="teamId")
	Set<Player> players;
	
	public Team() {
		super();
	
	}
	
	
	public Team(String locaion, String name, Set<Player> players) {
		this();
		this.locaion = locaion;
		this.name = name;
		this.players = players;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocaion() {
		return locaion;
	}
	public void setLocaion(String locaion) {
		this.locaion = locaion;
	}
	public String getMascoote() {
		return mascoote;
	}
	public void setMascoote(String mascoote) {
		this.mascoote = mascoote;
	}
	public Set<Player> getPlayers() {
		return players;
	}
	public void setPlayers(Set<Player> players) {
		this.players = players;
	}
	
	
}
