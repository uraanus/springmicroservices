package com.example.demo.domain;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.TeamDao;

@RestController
public class WhatEverRestController {

	@Autowired
	private TeamDao teamDao;
	
	
	
	@RequestMapping("/team/{name}")
	public Team sayHelloToTeam(@PathVariable String name) { // or you can make it @controller and add @responseBody before name
		return teamDao.findByName(name);
			
	}
}
