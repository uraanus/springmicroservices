package com.example.demo;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Controller
public class WhatEverIWant {

	@RequestMapping("/hi/{name}")
	public String sayHellou(Map model, @PathVariable String name) {
		model.put("name", name);
		return "hello";
	}
	
}
